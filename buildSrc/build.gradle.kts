plugins {
	`kotlin-dsl`
}

repositories {
	jcenter()
	google()
}

dependencies {
	implementation("com.android.tools.build:gradle:3.5.3") // up-to-date-androidGradleBuildTool
	implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.71") // up-to-date-kotlinVersion
}
