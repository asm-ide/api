# ASM
**Android Studio Mobile - A mobile IDE**

**CURRENTLY CONTAINING BUG AND DEVELOPING**

The ASM Team makes the android studio in the mobile device.
To contribute, see [contributing guides](CONTRIBUTING.md).

## Features
For more details, see each README.me files in each folder(like main/app)
### App UI
* material design

### Project code
* code API
* real-time code highlighter
* java code analysis plugin(local)

### Project apkBuilder
* compiler API
* android APK builder
* gradle support

### project block
in block-editor branch
* making java/android app in block coding
* block drag&drop
* java -> blocks, blocks -> java code converter

## Wiki
> [wiki link](https://github.com/asm-ide/ASM/wiki)

## Licenses
For details, see [HERE](LICENSE.md).
```
		
                        Project ASM
         © ASM Team 2017~2018. All Rights Reserved.
	
                          ASM code
           © LHW 2017~2018. All Rights Reserved.
                  package com.asm.widget.*
                 /com.asm.widget.codeedit.*
                To see detail licences, go to
                      core-ui/LICENSE.
	
                      ASM Compiler unit
              : Android Gradle Build Support
		  : APK Building In Android
		
               © GongBj. All Rights Reserved.
	
                     ASM android app UI
        © Yoon2 and PentiumG. All Rights Reserved.
	
                        ASM File IO
               © Yoon2. All Rights Reserved.
```
