package com.asm.io


@Experimental(level = Experimental.Level.ERROR)
annotation class AsFileExperimental


abstract class VirtualFile {
	abstract val fileSystem: VirtualFileSystem
	abstract val path: CharSequence
	
	abstract operator fun get(vararg children: String): VirtualFile
	
	@AsFileExperimental
	open fun asFilePath(): String = error("not implemented")
	
	val name: CharSequence get() = with(path) { takeLast(lastIndexOf(fileSystem.separator)) }
	
	fun rename(newPath: CharSequence) {
		if(newPath == path) return
		if(!fileSystem.impl.isValidPath(newPath))
			error("Invalid path: $newPath")
		fileSystem.impl.rename(path, newPath)
	}
	
	abstract fun read(): ReadStream
	abstract fun write(): Stream
}
