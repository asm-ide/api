package com.asm.io

import com.asm.util.SplitLock
import com.asm.util.atomic
import kotlin.math.min


// TODO: need more and more and more test
fun streamPipe(): Pair<ReadStream, Stream> {
	var tempBuffer by atomic<Buffer?>(null)
	val completeLock = SplitLock()
	val provideLock = SplitLock()
	
	var remainingUpdated = UnknownRemaining
	var isClosed = false
	
	return object : ReadStream() {
		override suspend fun read(buffer: Buffer): Int {
			if(isClosed) closedIoException()
			var offset = 0
			
			loop@ while(true) {
				// wait for writing
				if(tempBuffer == null) provideLock.wait()
				
				// if closed, hit the eof -> return result
				if(isClosed) return offset
				
				val temp = tempBuffer!!
				
				// read
				val srcLeft = temp.count
				val dstLeft = buffer.count - offset
				val count = min(srcLeft, dstLeft)
				
				temp.copyInto(destination = buffer, destinationOffset = offset, endIndex = count)
				offset += count
				
				when {
					// left
					srcLeft > dstLeft -> {
						// shrink the buffer
						tempBuffer = temp.copy(startIndex = temp.startIndex + count)
						break@loop
					}
					
					// fit
					srcLeft == dstLeft -> {
						tempBuffer = null
						break@loop
					}
					
					// insufficient
					else -> {
						tempBuffer = null
						// write() is waiting this function to finish
						completeLock.releaseAll()
					}
				}
			}
			
			completeLock.releaseAll()
			
			// reaching here means reading all the required data
			return buffer.count
		}
		
		override val remaining get() = remainingUpdated
		
		override suspend fun close() {
			isClosed = true
			
			// if waiting for it
			completeLock.releaseAll()
		}
		
	} to object : Stream() {
		override var remaining
			get() = remainingUpdated
			set(value) {
				remainingUpdated = value
			}
		
		override suspend fun write(buffer: Buffer) {
			if(isClosed) closedIoException()
			if(buffer.count == 0) return
			
			// check if not read yet
			while(tempBuffer != null) completeLock.wait()
			
			if(isClosed) closedIoException() // TODO: exception or silent?
			
			// hand in the buffer
			tempBuffer = buffer
			provideLock.releaseAll()
			
			// wait for read() to consume all the data
			completeLock.wait()
		}
		
		override suspend fun flush() {
			// nothing; write() itself flushes
		}
		
		override suspend fun close() {
			isClosed = true
			
			// if waiting for it
			provideLock.releaseAll()
		}
	}
}


suspend fun ReadStream.redirect(into: Stream) {
	readAll { buffer, count ->
		into.write(buffer, endIndex = count)
	}
}
