package com.asm.io

import com.asm.util.assert


inline val Buffer.count: Int get() = endIndex - startIndex

data class Buffer(val bytes: ByteArray, val startIndex: Int = 0, val endIndex: Int = bytes.size) : Iterable<Byte> {
	init {
		assert {
			when {
				startIndex < 0 -> "negative startIndex($startIndex)"
				startIndex > endIndex -> "startIndex($startIndex) > endIndex($endIndex)"
				endIndex > bytes.size -> "endIndex($endIndex) > buffer size(${bytes.size})"
				else -> return@assert
			}.also { throw IndexOutOfBoundsException(it) }
		}
	}
	
	@Suppress("NOTHING_TO_INLINE")
	inline operator fun get(index: Int): Byte {
		assert { if(index !in 0..count) throw IndexOutOfBoundsException() }
		return bytes[startIndex + index]
	}
	
	inline fun forEach(block: (Byte) -> Unit) {
		for(i in startIndex..endIndex) block(bytes[i])
	}
	
	@Suppress("NOTHING_TO_INLINE")
	@PublishedApi
	internal inline fun getUnchecked(index: Int) = bytes[startIndex + index]
	
	inline fun forEachIndexed(block: (index: Int, data: Byte) -> Unit) {
		for(i in 0..count) block(i, getUnchecked(i))
	}
	
	override fun equals(other: Any?): Boolean {
		if(this === other) return true
		if(other == null || this::class != other::class) return false
		other as Buffer
		
		for(i in indices)
			if(other.getUnchecked(i) != getUnchecked(i)) return false
		return true
	}
	
	override fun hashCode(): Int {
		var result = 1
		
		for(i in indices) {
			result = 31 * result + getUnchecked(i)
		}
		
		return result
	}
	
	override fun toString() = bytes.copyOfRange(startIndex, endIndex).contentToString()
	
	override fun iterator() = object : ByteIterator() {
		var i = startIndex
		override fun hasNext() = i < endIndex
		override fun nextByte() = bytes[i++]
	}
}


inline val Buffer.indices get() = 0..count

// TODO test
fun Buffer.copyInto(destination: Buffer, destinationOffset: Int = 0, startIndex: Int = 0, endIndex: Int = count) {
	copyInto(destination = destination.bytes,
			destinationStartIndex = destination.startIndex, destinationEndIndex = destination.endIndex,
			destinationOffset = destinationOffset,
			startIndex = startIndex, endIndex = endIndex)
}

fun Buffer.copyInto(destination: ByteArray,
		destinationStartIndex: Int = 0,
		destinationEndIndex: Int = destination.size,
		destinationOffset: Int = 0,
		startIndex: Int = 0,
		endIndex: Int = count) {
	/*
	 * src(t): [# # 2 3 4 5 6 7 8 9 # # # #]
	 *              ^ t.sI + sI.....^ t.sI + sE
	 *
	 * dst:    [# # # # # # # 7 8 9 10 11 12 # # #]
	 *                  ^ dSI                   ^ dEI
	 *                        ^ dSI + dO.....^ dSI + dO + c
	 */
	
	val srcStart = this.startIndex + startIndex
	val count = endIndex - startIndex
	
	assert {
		when {
			// source
			startIndex < 0 -> "negative startIndex($startIndex)"
			startIndex > endIndex -> "startIndex($startIndex) > endIndex($endIndex)"
			
			// destination
			destinationStartIndex < 0 -> "negative destinationStartIndex($destinationStartIndex)"
			destinationOffset !in 0..destinationEndIndex - destinationStartIndex ->
				"destinationOffset($destinationOffset) !in destination bound: count = (${destinationEndIndex - destinationStartIndex})"
			
			// mixed
			destinationOffset + count > destinationEndIndex ->
				"count exceeds destination size: destinationOffset($destinationOffset) + count($count) > destinationEndIndex($destinationEndIndex)"
			
			else -> return@assert
		}.also { throw IndexOutOfBoundsException(it) }
	}
	
	bytes.copyInto(destination,
			destinationOffset = destinationStartIndex + destinationOffset,
			startIndex = srcStart,
			endIndex = srcStart + count)
}

