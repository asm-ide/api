package com.asm.io


interface Pipe<I, O> {
	fun connect(next: O): I
}
