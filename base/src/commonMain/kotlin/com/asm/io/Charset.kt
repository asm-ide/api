package com.asm.io


expect abstract class Charset {
	fun name(): String
}


expect fun charsetForName(charsetName: String): Charset


object Charsets {
	val UTF_8: Charset get() = charsetForName("UTF_8")
	
	val UTF_16: Charset get() = charsetForName("UTF_16")
	
	val UTF_16BE: Charset get() = charsetForName("UTF_16BE")
	
	val UTF_16LE: Charset get() = charsetForName("UTF_16LE")
	
	val US_ASCII: Charset get() = charsetForName("US_ASCII")
	
	val ISO_8859_1: Charset get() = charsetForName("ISO_8859_1")
}
