package com.asm.io

import com.asm.internal.Throws


interface Closeable {
	@Throws(IoException::class)
	suspend fun close()
}


suspend inline fun <T : Closeable, R> T.use(block: T.() -> R): R {
	try {
		return block()
	} finally {
		close()
	}
}
