package com.asm.io

import com.asm.internal.Throws

const val UnknownRemaining = -1


/**
 * Stream represents a one-way flow of bytes.
 *
 * A stream can be used to both read and write a file.
 * In case of reading, bytes flow from the content of the file to the consumer, and in case of writing,
 * from the consumer to the content.
 */
abstract class Stream : Flushable, Closeable {
	/**
	 * A way for implementation of this stream to estimate the number of remaining bytes.
	 * -1 means not knowing how many bytes are left.
	 */
	open var remaining: Int
		@Throws(IoException::class) get() = UnknownRemaining // meaning undefined
		@Throws(IoException::class) set(_) {}
	
	@Throws(IoException::class)
	suspend inline fun write(buffer: ByteArray, startIndex: Int = 0, endIndex: Int = buffer.size) {
		write(Buffer(buffer, startIndex, endIndex))
	}
	
	@Throws(IoException::class)
	abstract suspend fun write(buffer: Buffer)
}
