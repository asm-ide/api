package com.asm.io


val VirtualFile.extension: CharSequence
	get() = name.run {
		val index = lastIndexOf('.')
		if(index == -1) "" else takeLast(index + 1)
	}

val VirtualFile.nameWithoutExtension: CharSequence
	get() = name.run {
		val index = lastIndexOf('.')
		if(index == -1) name else take(index)
	}