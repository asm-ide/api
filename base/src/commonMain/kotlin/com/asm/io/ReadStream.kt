package com.asm.io

import com.asm.internal.Throws


abstract class ReadStream : Closeable {
	/**
	 * Estimation of the number of remaining bytes.
	 * -1 means not knowing the number, so try [read]ing the bytes.
	 */
	open val remaining: Int @Throws(IoException::class) get() = UnknownRemaining
	
	/**
	 * Read some of remaining bytes into buffer.
	 *
	 * @return the number of read bytes
	 */
	@Throws(IoException::class)
	abstract suspend fun read(buffer: Buffer): Int
	
	@Throws(IoException::class)
	suspend inline fun read(buffer: ByteArray, startIndex: Int = 0, endIndex: Int = buffer.size): Int =
			read(Buffer(buffer, startIndex, endIndex))
}


suspend fun ReadStream.read(count: Int): ByteArray {
	val buffer = Buffer(ByteArray(count))
	read(buffer)
	return buffer.bytes
}

suspend inline fun ReadStream.readAll(callback: (buffer: ByteArray, count: Int) -> Unit) {
	val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
	
	while(true) {
		val count = read(buffer)
		callback(buffer, count)
		if(count != buffer.size) break
	}
}
