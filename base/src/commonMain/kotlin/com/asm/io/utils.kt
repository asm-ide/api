@file:JvmName("IoUtils")

package com.asm.io

import kotlin.jvm.JvmName


/**
 * The default buffer size when working with buffered streams.
 */
const val DEFAULT_BUFFER_SIZE: Int = 8 * 1024

