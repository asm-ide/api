package com.asm.io


abstract class VirtualFileSystem {
	abstract val separator: Char
	
	
	abstract val impl: VirtualFileImpl
	
	abstract class VirtualFileImpl {
		abstract fun rename(oldPath: CharSequence, newPath: CharSequence)
		abstract fun isValidPath(path: CharSequence): Boolean
		abstract fun isValidName(path: CharSequence): Boolean
	}
}
