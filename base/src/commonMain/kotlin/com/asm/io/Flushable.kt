package com.asm.io


interface Flushable {
	suspend fun flush()
}
