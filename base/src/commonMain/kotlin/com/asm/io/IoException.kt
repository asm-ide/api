package com.asm.io


expect open class IoException(message: String? = null, cause: Throwable? = null) : Exception


expect class ClosedIoException() : IoException


fun ioException(message: String? = null, cause: Throwable? = null): Nothing =
		throw IoException(message, cause)

fun closedIoException(): Nothing = throw ClosedIoException()

