package com.asm.annotation


/**
 * Shows that class is included in API.
 * This API is part of ASM, and can be accessed by application or plugin.
 * Also, public or protected methods will not be optimised or renamed by proguard or others.
 */
@MustBeDocumented
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class API
