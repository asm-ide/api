package com.asm.util

import kotlin.properties.ReadWriteProperty


expect inline fun <T> threadLocal(noinline initialValue: () -> T): ReadWriteProperty<Any?, T>
