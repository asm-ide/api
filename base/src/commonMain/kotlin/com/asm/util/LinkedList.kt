package com.asm.util


expect inline fun <T> linkedListOf(): MutableList<T>

expect inline fun <T> linkedListOf(vararg items: T): MutableList<T>