package com.asm.util

import com.asm.io.ReadStream
import kotlin.reflect.KClass


abstract class ClassLoader {
	abstract fun <T : Any> findClass(name: String): KClass<T>
	abstract fun openResource(path: String): ReadStream
}
