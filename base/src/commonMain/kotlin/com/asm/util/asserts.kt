package com.asm.util


expect inline fun assert(block: () -> Unit)

expect inline fun assert(condition: Boolean)

expect inline fun assert(condition: Boolean, lazyMessage: () -> String)

expect inline fun assertThrow(condition: Boolean, throwBlock: () -> Throwable)

inline fun <R> asserted(condition: Boolean, block: () -> R): R {
	assert(condition)
	return block()
}
