package com.asm.util

class StringIndexOutOfBoundsException(message: String) : Throwable(message)