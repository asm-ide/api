@file:JvmName("LogUtils")

package com.asm.util

import kotlin.jvm.JvmName
import kotlin.jvm.JvmOverloads


// TODO: create context stack for several usages.


enum class Level { verbose, debug, warn, error }


@Suppress("ArrayInDataClass")
data class LogContext internal constructor(
		val tag: String?,
		val baseLevel: Level,
		val isMuted: Boolean = false
) {
	fun inherit(tag: String? = null,
				baseLevel: Level? = null,
				isMuted: Boolean = false
	) = LogContext(
			tag = tag ?: this.tag,
			baseLevel = baseLevel ?: this.baseLevel,
			isMuted = isMuted || this.isMuted
	)
}


typealias LogTagGenerator = (defaultTag: String?) -> String

expect fun createDefaultLogTagGenerator(): LogTagGenerator

var logTagGenerator: LogTagGenerator = createDefaultLogTagGenerator()


expect val logContext: LogContext


/**
 * Utility function for using [log]. Basically when using [log], it automatically collects some
 * context, but you can provide more specific and detail log context via this function.
 *
 * If you use java, you can also use [logContextBegin], if you can't use lambda syntax.
 */
inline fun <R> logContext(tag: String? = null,
						  level: Level? = null,
						  isMuted: Boolean = false,
						  block: () -> R
) = try {
	logContextBegin(tag, level, isMuted)
	block()
} finally {
	logContextEnd()
}

/**
 * Utility function for using [log]. Basically when using [log], it automatically collects some
 * context, but you can provide more specific and detail log context via this function.
 *
 * This function notes that log tag context begins here.
 * If you use kotlin, don't use this function. Instead, use [logContext].
 *
 * You **should call [logContextEnd]** after the end of the context, for example when exiting the
 * function. **When you don't call it and [log] is used after exiting the function where
 * [logContextBegin] is called, [logContextEnd] is automatically called**, but it is hard to track
 * the context so you should call [logContextEnd].
 *
 * ```java
 * void myLogFunction() {
 *     LogUtils.logContextBegin("ASM");
 *     Log.d("Hello, world"); // first
 * }
 *
 * void consume() {
 *     myLogFunction();
 *     Log.d("Hello programmer!"); // second
 *     LogUtils.logContextEnd(); // has no effects
 * }
 * ```
 *
 * ```
 * Result: (pseudo output)
 *
 * [ASM] Hello, world
 * void consume(): Hello programmer!
 * ```
 *
 * In this situation, first one is logged with log tag "ASM", but second one is logged without log
 * tag and hard to track log tag. So, [logContextBegin], and [logContextEnd] should be in one
 * function, like this:
 * ```java
 * void myLogFunction() {
 *     LogUtils.logContextBegin("ASM");
 *     Log.d("Hello, world"); // first
 *     LogUtils.logContextEnd();
 * }
 *
 * void consume() {
 *     myLogFunction();
 *     Log.d("Hello programmer!"); // second
 * }
 * ```
 * This succeeds as you expect.
 * Alternatively, you can also use [logContext] like this:
 *
 * ```java
 * void myLogFunction() {
 *     LogUtils.logContext("ASM", () -> { Log.d("Hello, world"); });
 * }
 *
 * void consume() {
 *     myLogFunction();
 *     Log.d("Hello programmer!"); // second
 * }
 * ```
 * If you use Java 8(or above) for developing, this might be much better.
 *
 * @see logContext
 * @see logContextEnd
 */
@JvmOverloads
expect fun logContextBegin(tag: String? = null, level: Level? = null, isMuted: Boolean = false): LogContext

/**
 * Utility function for using [log]. Basically when using [log], it automatically collects some
 * context, but you can provide more specific and detail log context via this function.
 *
 * This function notes that log tag context ends here.
 * If you use kotlin, don't use this function. Instead, use [logContext].
 *
 * @see logContext
 * @see logContextBegin
 */
expect fun logContextEnd()

inline fun <R> mute(block: () -> R) = logContext(isMuted = true, block = block)

@JvmOverloads
expect fun log(message: String, throwable: Throwable? = null)

@JvmOverloads
expect fun log(level: Level, message: String, throwable: Throwable? = null)

internal expect fun log(context: LogContext, level: Level, message: String, throwable: Throwable?)


@JvmOverloads
fun logV(message: String, throwable: Throwable? = null) {
	log(Level.verbose, message, throwable)
}


fun logger() = Logger(logContext.inherit(tag = logTagGenerator(logContext.tag)))


class Logger(val context: LogContext) {
	constructor(tag: String? = null, level: Level? = null) : this(logContext.inherit(tag = tag, baseLevel = level))
	
	
	@JvmOverloads
	fun log(message: String, throwable: Throwable? = null) {
		log(context, context.baseLevel, message, throwable)
	}
	
	/**
	 * Prints a log with log level verbose.
	 */
	@JvmOverloads
	fun v(message: String, throwable: Throwable? = null) {
		log(context, Level.verbose, message, throwable)
	}
	
	/**
	 * Prints a log with log level debug.
	 */
	@JvmOverloads
	fun d(message: String, throwable: Throwable? = null) {
		log(context, Level.debug, message, throwable)
	}
	
	/**
	 * Prints a log with log level warn.
	 */
	@JvmOverloads
	fun w(message: String, throwable: Throwable? = null) {
		log(context, Level.warn, message, throwable)
	}
	
	/**
	 * Prints a log with log level error.
	 */
	@JvmOverloads
	fun e(message: String, throwable: Throwable? = null) {
		log(context, Level.error, message, throwable)
	}
}