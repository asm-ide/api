package com.asm.util

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock


class SplitLock {
	private val mutexList = mutableListOf<Mutex>()
	
	
	suspend fun wait() {
		val lock = Mutex(true)
		mutexList += lock
		lock.withLock {}
	}
	
	fun release() {
		mutexList.removeAt(0).unlock()
	}
	
	fun releaseAll() {
		mutexList.forEach {
			it.unlock()
		}
		
		mutexList.clear()
	}
}
