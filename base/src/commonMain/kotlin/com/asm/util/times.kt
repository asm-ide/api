package com.asm.util


expect inline fun currentTimeMillis(): Long

expect inline fun currentNanoTime(): Long