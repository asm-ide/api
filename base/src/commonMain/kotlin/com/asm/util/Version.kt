package com.asm.util


data class Version(var code: Int = 0, var name: String)
