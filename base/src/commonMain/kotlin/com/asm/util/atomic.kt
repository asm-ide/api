@file:Suppress("NOTHING_TO_INLINE")

package com.asm.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


// Downside of these: there aren't any fast operations such as compareAndSet

inline fun <T> atomic(initialValue: T) = AtomicValue(initialValue)

inline fun atomic(initialValue: Byte) = AtomicByte(initialValue)

inline fun atomic(initialValue: Short) = AtomicShort(initialValue)

inline fun atomic(initialValue: Int) = AtomicInt(initialValue)

inline fun atomic(initialValue: Long) = AtomicLong(initialValue)

inline fun atomic(initialValue: Float) = AtomicFloat(initialValue)

inline fun atomic(initialValue: Double) = AtomicDouble(initialValue)

inline fun atomic(initialValue: Char) = AtomicChar(initialValue)

inline fun atomic(initialValue: Boolean) = AtomicBoolean(initialValue)


expect class AtomicValue<T>(initialValue: T) : ReadWriteProperty<Any?, T>

expect class AtomicByte(initialValue: Byte) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Byte
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Byte)
}

expect class AtomicShort(initialValue: Short) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Short
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Short)
}

expect class AtomicInt(initialValue: Int) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Int
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int)
}

expect class AtomicLong(initialValue: Long) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Long
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long)
}

expect class AtomicFloat(initialValue: Float) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Float
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Float)
}

expect class AtomicDouble(initialValue: Double) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Double
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Double)
}

expect class AtomicChar(initialValue: Char) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Char
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Char)
}

expect class AtomicBoolean(initialValue: Boolean) {
	operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean
	operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean)
}

