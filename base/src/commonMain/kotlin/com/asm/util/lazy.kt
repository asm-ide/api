package com.asm.util


@Suppress("NOTHING_TO_INLINE")
inline fun <T> lazyNone(noinline init: () -> T) = lazy(LazyThreadSafetyMode.NONE, init)
