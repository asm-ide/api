package com.asm.util

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import kotlin.coroutines.CoroutineContext


abstract class TaskHandler {
	abstract fun post(block: () -> Unit)
	abstract fun postDelayed(timeMillis: Long, block: () -> Unit)
	
	val dispatcher: CoroutineDispatcher = object : CoroutineDispatcher() {
		override fun dispatch(context: CoroutineContext, block: Runnable) {
			post { block.run() }
		}
		
		
	}
}
