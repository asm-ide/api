package com.asm.util


var isAsmDebugEnabled = false


inline fun debug(block: () -> Unit) {
	if(isAsmDebugEnabled) block()
}