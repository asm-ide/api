package com.asm.io

import java.nio.charset.Charset as JCharset


actual typealias Charset = JCharset

@Suppress("NOTHING_TO_INLINE")
actual inline fun charsetForName(charsetName: String): Charset = JCharset.forName(charsetName)
