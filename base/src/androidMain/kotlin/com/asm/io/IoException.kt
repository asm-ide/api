@file:JvmName("_IoException")

// workaround for https://youtrack.jetbrains.com/issue/KT-21186
package com.asm.io


actual typealias IoException = java.io.IOException

actual class ClosedIoException : IoException()
