@file:JvmName("LogUtilsJvm") // workaround for https://youtrack.jetbrains.com/issue/KT-21186
@file:JvmMultifileClass

package com.asm.util


// TODO: automatically track context on lifecycle event on Plugin / etc
// TODO: when showing caller method name and info, alter by the language(kotlin/java: via @Metadata)

import android.util.Log as ALog


private class DummyClass

private val sPackageName = DummyClass::class.java.name.substringBeforeLast(".")


actual fun createDefaultLogTagGenerator(): LogTagGenerator = if(isAsmDebugEnabled) { defaultTag ->
	Throwable().stackTrace.first {
		!it.className.startsWith(sPackageName)
	}.toLogString().let { if(defaultTag == null) it else "$defaultTag/$it" }
			.let { if(it.length > 23) it.substring(0, 23) else it }
} else { defaultTag -> defaultTag ?: "Log" }

private fun StackTraceElement.toLogString() = "${className.substringAfterLast('.')}.$methodName"


private val contextStack by threadLocal { mutableListOf(LogContext(tag = "Log", baseLevel = Level.verbose)) }

private val sLogContextStub = LogContext(tag = "LogStub", baseLevel = Level.verbose)

actual val logContext: LogContext get() = if(isAsmDebugEnabled) contextStack.last() else sLogContextStub

actual fun logContextBegin(tag: String?, level: Level?, isMuted: Boolean) =
		if(isAsmDebugEnabled) logContext.inherit(tag = tag, baseLevel = level, isMuted = isMuted).also {
			contextStack += it
		} else sLogContextStub

actual fun logContextEnd() {
	if(isAsmDebugEnabled)
		contextStack.removeAt(contextStack.size - 1)
}


actual fun log(message: String, throwable: Throwable?) {
	val context = logContext
	log(context, context.baseLevel, message, throwable)
}

actual fun log(level: Level, message: String, throwable: Throwable?) {
	log(logContext, level, message, throwable)
}

@Suppress("NOTHING_TO_INLINE")
internal actual inline fun log(context: LogContext, level: Level, message: String, throwable: Throwable?) {
	if(!isAsmDebugEnabled) return
	
	val tag = logTagGenerator(context.tag)
	
	val priority = when(level) {
		Level.verbose -> ALog.VERBOSE
		Level.debug -> ALog.DEBUG
		Level.warn -> ALog.WARN
		Level.error -> ALog.ERROR
	}
	
	ALog.println(priority, tag,
			throwable?.let { "$message\n${ALog.getStackTraceString(it)}" } ?: message)
}
