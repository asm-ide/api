@file:Suppress("NOTHING_TO_INLINE")

package com.asm.util

import java.util.LinkedList

actual inline fun <T> linkedListOf(): MutableList<T> = LinkedList()

actual inline fun <T> linkedListOf(vararg items: T): MutableList<T> =
		LinkedList<T>().apply { addAll(items) }