@file:Suppress("NOTHING_TO_INLINE")

package com.asm.util


actual inline fun currentTimeMillis(): Long = System.currentTimeMillis()

actual inline fun currentNanoTime(): Long = System.nanoTime()