package com.asm.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


@Suppress("NOTHING_TO_INLINE")
actual inline fun <T> threadLocal(noinline initialValue: () -> T): ReadWriteProperty<Any?, T> =
		ThreadLocal(initialValue)

class ThreadLocal<T>(initialValue: () -> T) : ReadWriteProperty<Any?, T> {
	private val threadLocal = object : java.lang.ThreadLocal<T>() {
		override fun initialValue() = initialValue()
	}
	
	override fun getValue(thisRef: Any?, property: KProperty<*>): T = threadLocal.get()!!
	
	override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
		threadLocal.set(value)
	}
	
}
