package com.asm.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


actual class AtomicValue<T> actual constructor(initialValue: T) : ReadWriteProperty<Any?, T> {
	@Volatile
	private var value = initialValue
	
	override fun getValue(thisRef: Any?, property: KProperty<*>): T = value
	
	override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
		this.value = value
		
	}
}


actual class AtomicByte actual constructor(initialValue: Byte) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Byte = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Byte) {
		this.value = value
		
	}
}

actual class AtomicShort actual constructor(initialValue: Short) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Short = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Short) {
		this.value = value
		
	}
}

actual class AtomicInt actual constructor(initialValue: Int) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Int = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) {
		this.value = value
		
	}
}

actual class AtomicLong actual constructor(initialValue: Long) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Long = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Long) {
		this.value = value
		
	}
}

actual class AtomicFloat actual constructor(initialValue: Float) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Float = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Float) {
		this.value = value
		
	}
}

actual class AtomicDouble actual constructor(initialValue: Double) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Double = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Double) {
		this.value = value
		
	}
}

actual class AtomicChar actual constructor(initialValue: Char) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Char = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Char) {
		this.value = value
		
	}
}

actual class AtomicBoolean actual constructor(initialValue: Boolean) {
	@Volatile
	private var value = initialValue
	
	actual operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean = value
	
	actual operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) {
		this.value = value
		
	}
}
