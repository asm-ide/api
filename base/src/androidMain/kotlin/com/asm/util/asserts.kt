@file:JvmName("_asserts") // workaround for https://youtrack.jetbrains.com/issue/KT-21186

package com.asm.util

import kotlin.contracts.contract


// This file is an effort to reduce the result of calling these inline assertion functions
// The name will be shrunk by the proguard


@PublishedApi
internal object AssertionStatus {
	@JvmField
	@PublishedApi
	internal val enabled = javaClass.desiredAssertionStatus()
}


actual inline fun assert(block: () -> Unit) {
	if(AssertionStatus.enabled) block()
}

@Suppress("NOTHING_TO_INLINE")
actual inline fun assert(condition: Boolean) {
	contract {
		returns() implies condition
	}
	
	if(AssertionStatus.enabled && !condition) throwAssertionError()
}

@PublishedApi
internal fun throwAssertionError(): Unit = throw AssertionError()
// the return type is Unit because if it is Nothing, kotlin compiler adds 'throw null;'
// : to reduce the amount of code


actual inline fun assert(condition: Boolean, lazyMessage: () -> String) {
	contract {
		returns() implies condition
	}
	
	if(AssertionStatus.enabled && !condition) throw AssertionError(lazyMessage())
}

actual inline fun assertThrow(condition: Boolean, throwBlock: () -> Throwable) {
	contract {
		returns() implies condition
	}
	
	if(AssertionStatus.enabled && !condition) throw throwBlock()
}
