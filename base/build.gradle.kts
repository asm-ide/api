plugins {
	kotlin("multiplatform")
	
	// android target
	id("com.android.library")
	
	id("com.asm.build.common-plugin")
}
